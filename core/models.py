from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse

# Users: name, phone, test_status: (running, sick, not sick, unsure)

class QuestionGroup(models.Model):
    """ QuestionGroup : name, description """

    name = models.CharField(_("Name"), max_length=50)
    description = models.TextField(_("description"))

    class Meta:
        verbose_name = _("questiongroup")
        verbose_name_plural = _("questiongroups")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("questiongroup_detail", kwargs={"pk": self.pk})


class Question(models.Model):
    """ Question : name, fr_version, en_version, description, order, group """

    name = models.CharField(_("Name"), max_length=50)
    fr_version = models.TextField(_("FR Version"))
    en_version = models.TextField(_("EN Version"))
    description = models.TextField(_("Description"))
    position = models.IntegerField(_("Order"))
    group = models.ForeignKey("core.QuestionGroup", verbose_name=_("Question Group"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("question")
        verbose_name_plural = _("questions")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("question_detail", kwargs={"pk": self.pk})


class Answer(models.Model):
    """ Answer : question, answer, points """
    question = models.ForeignKey("core.Question", verbose_name=_("Question"), on_delete=models.CASCADE)
    text_fr = models.CharField(_("Text FR"), max_length=50)
    text_en = models.CharField(_("Text EN"), max_length=50)
    points = models.IntegerField(_("Points"))

    class Meta:
        verbose_name = _("answer")
        verbose_name_plural = _("answers")

    def __str__(self):
        return self.text_fr + ' - ' + str(self.points)

    def get_absolute_url(self):
        return reverse("answer_detail", kwargs={"pk": self.pk})


class ResponseGroup(models.Model):

    name = models.CharField(_("Name"), max_length=50)
    question_group = models.ForeignKey("core.QuestionGroup", verbose_name=_("Question Group"), on_delete=models.CASCADE)
    phone = models.CharField(_("Phone Number"), max_length=50)
    status = models.CharField(_("Status"), max_length=50)
    progress = models.IntegerField(_("Progress"), default=1)
    nb_question = models.IntegerField(_("Number Of Questions"))
    result = models.CharField(_("Test result"), max_length=50, default="unknown")
    points = models.IntegerField(_("Points"), default=0)

    class Meta:
        verbose_name = _("responsegroup")
        verbose_name_plural = _("responsegroups")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("responsegroup_detail", kwargs={"pk": self.pk})


class Response(models.Model):
    """ Response : group, position, text """
    
    group = models.ForeignKey("core.ResponseGroup", verbose_name=_("Response Group"), on_delete=models.CASCADE)
    position = models.IntegerField(_("Position"))
    text = models.CharField(_("Text"), max_length=50)

    class Meta:
        verbose_name = _("response")
        verbose_name_plural = _("responses")

    def __str__(self):
        return self.text

    def get_absolute_url(self):
        return reverse("response_detail", kwargs={"pk": self.pk})


class Message(models.Model):
    """ Message : from, to, text  """

    from_num = models.CharField(_("From"), max_length=50)
    to_num = models.CharField(_("To"), max_length=50)
    sms_text = models.TextField(_("SMS Text"))
    timestamp = models.DateTimeField(_("Created at"), auto_now=False, auto_now_add=False)

    class Meta:
        verbose_name = _("message")
        verbose_name_plural = _("messages")

    def __str__(self):
        return self.timestamp.strftime("%d-%m-%Y %H.%M") + ' : ' + self.from_num + ' : ' + self.to_num + ' : ' + self.sms_text

    def get_absolute_url(self):
        return reverse("message_detail", kwargs={"pk": self.pk})
