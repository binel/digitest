from django.shortcuts import render
from django.http import JsonResponse
import json
from datetime import datetime

from .models import QuestionGroup, Question, Answer, ResponseGroup, Response, Message


# Create your views here.
def test(request):

    if request.method == 'GET':
        data = {
            'status': 'OK',
            'code': 200,
            'message': 'You have made a GET request'
        }
        return JsonResponse(data)


    elif request.method == 'POST':
        covid_group_id = 1
        
        # 1. Récupération du message reçu
        phone, message = receive_message(request.POST)

        if message == 'TEST':
            # If the number doesn't have any session 
            session_list = ResponseGroup.objects.filter(phone__icontains=phone).filter(status__icontains="ONGOING")

            if session_list.count() == 0:
                return start_session(phone, covid_group_id)
            else:
                # Get the Ongoing session and resend the last question
                session = session_list[0]

                if session.progress is not session.nb_question + 1:
                    question = Question.objects.get(position=session.progress)
                    return ask_correct_answer(phone, question)
                else:
                    return ask_to_start_test(phone)                

        else:
            # Get the Session of the message sent
            session_list = ResponseGroup.objects.filter(phone__icontains=phone).filter(status__icontains="ONGOING")

            if session_list.count() == 0:
                return ask_to_start_test(phone)                
            else:
                # Get the Ongoing session
                session = session_list[0]
                progress = session.progress
                points = session.points

                # Get the actual question
                question = Question.objects.get(position=progress)

                # Get the list of answers of the question
                answers = Answer.objects.filter(question=question)
                
                # Check that the message coresponds to one answer
                check = False
                for answer in answers:
                    if answer.text_fr == message:
                        # Update the points of the answer
                        session.points = points + answer.points
                        # Update the Session progress to the next question
                        session.progress = progress + 1
                        session.save()
                        check = True
                
                if check == False:
                    return ask_correct_answer(phone, question)
                else:
                    # Check if last Question is answered
                    if session.progress == session.nb_question + 1:
                        # Then calculate the test status
                        return calculate_status(session)

                    else:
                        return send_question(session)


def start_session(phone, covid_grp_id):
    """ This function starts the session of the phone with the web server """

    # 1. Initiate the Session related to this phone number
    # If the number doesn't have any session create one
    session_list = ResponseGroup.objects.filter(phone__icontains=phone).filter(status__icontains="ONGOING")

    if session_list.count() == 0:
        nb_questions = Question.objects.filter(group=covid_grp_id).count()
        group = QuestionGroup.objects.get(id=covid_grp_id)

        resp_grp = ResponseGroup.objects.create(
            name=phone,
            question_group=group,
            phone=phone,
            status="STARTED",
            progress=1,
            nb_question=nb_questions,
            result="unknown"
        )
    
    # 1. Set the language of the Session
    started_list = ResponseGroup.objects.filter(phone__icontains=phone).filter(status__icontains="STARTED")
    
    if session_list.count() != 0:
        return ask_language(phone)

    # 1. Update the Session related to this phone number
    nb_questions = Question.objects.filter(group=covid_grp_id).count()
    group = QuestionGroup.objects.get(id=covid_grp_id)

    resp_grp = ResponseGroup.objects.create(
        name=phone,
        question_group=group,
        phone=phone,
        status="ONGOING",
        progress=1,
        nb_question=nb_questions,
        result="unknown"
    )




    # 2. Get the first Question and send it
    return send_question(resp_grp)


def ask_language(phone):
    message = "DIGITEST \nCOVID-19 TEST\n"
    message = "Veuillez choisir votre langue préférée./Choose your prefered language \n: FRANCAIS / ENGLISH"

    data = {
        "to_num": phone,
        "message": message
    }

    return send_message(data)


def send_question(session):
    position = session.progress
    question = Question.objects.get(position=position)
    phone = session.phone

    message = "DIGITEST \nCOVID-19 TEST\n"
    message = message + "Q" + str(position) + '/' + session.nb_question + '\n'
    message = message + question.fr_version

    data = {
        "to_num": phone,
        "message": message
    }

    return send_message(data)


def ask_to_start_test(phone):
    message = "DIGITEST \nCOVID-19 TEST\n"
    message = message + "Vous n'avez pas de test en cours. Veuillez envoyer TEST au 691388922 pour faire votre test"
    
    data = {
        "to_num": phone,
        "message": message
    }

    return send_message(data)


def ask_correct_answer(phone, question):
    message = "DIGITEST \nCOVID-19 TEST\n"
    message = message + "Réponse incorrecte. Veuillez choisir une des réponses proposées.\n " + question.fr_version

    data = {
        "to_num": phone,
        "message": message
    }

    return send_message(data)


def calculate_status(session):
    phone = session.phone
    status = session.status
    result = session.result
    points = session.points

    message = "DIGITEST \nCOVID-19 TEST\n"
    message = message + "Fin du test\n"

    session.status = "CLOSED"

    # The Subject is not sick
    if points <= 5:
        message = message + "Vous ne présentez pas les symptômes du COVID-19."
        result = "not sick"

    # We don't know
    elif 6 <= points and points <= 14 :    
        message = message + "Vous etes un cas ambigu. \nVeuillez vous faire depister dans le centre de sante le plus proche."
        result = "unknown"

    # The Subject is sick
    else:
        message = message + "Vous presentez 80 pour cents des symptomes des malades du COVID-19. \nVeuillez vous faire depister."
        result = "sick"
    
    session.result = result
    session.save()

    data = {
        "to_num": phone,
        "message": message
    }

    return send_message(data)


def send_message(data):

    mess = Message.objects.create(
        from_num="691388922",
        to_num=data['to_num'],
        sms_text=data['message'],
        timestamp=datetime.now()
    )

    print(mess)

    return JsonResponse(data)


def receive_message(data):
    new_data = { 
        'from_num': data.get('phone'), 
        'message': data.get('message')
    }

    mess = Message.objects.create(
        from_num=new_data['from_num'],
        to_num="691388922",
        sms_text=new_data['message'],
        timestamp=datetime.now()
    )
    print(mess)

    return new_data['from_num'], new_data['message']

